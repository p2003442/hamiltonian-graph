import random
import numpy as np
import sys
import time
from utils import file_to_graph, cycle_cost
class ACO:
    def __init__(
        self,
        v_init=None,
        it=2000,
        time=None,
        nb_ant=100,
        fact_pherom=10,
        fact_heurist=20,
        tau_evap=0.01,
        min_pherom=0.01,
        max_pherom=0.9,
    ):
        """constructor of ACO

        Args:
            v_init (int): defaults to None, node to begin.
            it (int): default to 1000.
            nb_ant (int, optional): . Defaults to 20.
            fact_pherom (int, optional): . Defaults to 1.
            fact_heurist (int, optional): . Defaults to 1.
            tau_evap (float, optional): . Defaults to 0.0001.
            min_pherom (float, optional): . Defaults to 0.1.
            max_pherom (float, optional): . Defaults to 0.9.
        """
        self.v_init = v_init
        self.nb_ant = nb_ant
        self.it = it
        self.time = time
        self.fact_pherom = fact_pherom
        self.fact_heurist = fact_heurist
        self.tau_evap = tau_evap
        self.min_pherom = min_pherom
        self.max_pherom = max_pherom

    def cycle(self, v_init=None):
        """compute all ant's path

        Returns:
            list:list of paths
        """
        all_cycle = []

        def p(v_i, v_j, already_travel):
            """compute proba to select the j node

            Args:
                v_i (int): current node
                v_j (int): evaluate node
                already_travel (list): path in construction

            Returns:
                float: prob
            """
            return (
                pow(self.matrix_pherom[v_i, v_j], self.fact_pherom)
                * (pow(1 / self.matrix[v_i][v_j], self.fact_heurist))
            ) / (
                sum(
                    [
                        (
                            pow(self.matrix_pherom[v_i, v_k], self.fact_pherom)
                            * (pow(1 / self.matrix[v_i][v_k], self.fact_heurist))
                        )
                        for v_k in [k for k in self.nodes if k not in already_travel]
                    ]
                )
            )

        probability_v_j = {}

        if self.time is not None:
            if time.time() - self.beggin >= self.time:
                raise Exception("out of time")


        for f in range(self.nb_ant):
            if self.time is not None:
                if time.time() - self.beggin >= self.time:
                    return all_cycle
            # print("fourmi ", f)
            if v_init is None:
                v_init = random.choice(self.nodes)
            already_travel = [v_init]
            v_i = v_init
            while len(already_travel) < len(self.nodes):
                # print(len(already_travel))
                nodes_to_visit = [
                    node
                    for node in self.nodes
                    if node not in already_travel and node != v_i
                ]
                next_v_i = nodes_to_visit[0]
                distrib_prob = []
                for j in nodes_to_visit:
                    if (v_i, j) not in probability_v_j.keys():
                        prob = p(v_i, j, already_travel)
                        # print(v_i, j, prob)
                        probability_v_j[(v_i, j)] = prob
                    distrib_prob.append(probability_v_j[(v_i, j)])
                    # if probability_v_j[(v_i, j)] > probability_v_j[(v_i, next_v_i)]:
                    #     next_v_i = j
                next_v_i = random.choices(nodes_to_visit, weights=distrib_prob)
                v_i = next_v_i[0]
                already_travel.append(v_i)

            # already_travel.append(v_init)
            all_cycle.append(already_travel)
            # print(already_travel)
        return all_cycle

    def maj_pherom(self, best_cycle):
        def consecutive_pairs(lst):
            return [(lst[i], lst[i + 1]) for i in range(len(lst) - 1)]

        for i, j in consecutive_pairs(best_cycle):
            self.matrix_pherom[i][j] += 1 / self.matrix[i][j]
            if self.matrix_pherom[i][j] > self.max_pherom:  # borne max
                self.matrix_pherom[i][j] = self.max_pherom

        self.matrix_pherom = (1 - self.tau_evap) * self.matrix_pherom  # evaporation

    def resolv_div_by_0(self, matrix):
        for i in range(len(matrix)):
            for j in range(len(matrix[0])):
                if matrix[i][j] == 0 and i != j:
                    matrix[i][j] = 0.0000001
        return matrix

    def solv(self, matrix):
        self.matrix = self.resolv_div_by_0(matrix)
        self.nodes = range(len(self.matrix))
        self.matrix_pherom = np.full_like(self.matrix, self.min_pherom, dtype=float)
        all_best_pref = []
        best_of_the_best_p = float("+inf")
        best_of_the_best_c = None
        if self.time is not None:
            self.beggin = time.time()
        for it in range(self.it):
            try:
                all_cycles = self.cycle(self.v_init)
            except Exception as e:
                print(str(e))
                break
            best_pref = float("+inf")
            best_cycle = None
            for cycle in all_cycles:
                pref = cycle_cost(self.matrix, cycle)
                if pref < best_pref:
                    best_pref = pref
                    best_cycle = cycle
            all_best_pref.append(best_pref)
            # print(best_cycle, best_pref)
            self.maj_pherom(best_cycle)

            if best_pref < best_of_the_best_p:
                best_of_the_best_p = best_pref
                best_of_the_best_c = best_cycle
        return best_of_the_best_c, all_best_pref

if __name__ == "__main__":
    if len(sys.argv) < 2:
        mat =[
                [0, 2451, 713, 1018, 1631, 1374, 2408, 213, 2571, 875, 1420, 2145, 1972],
                [2451, 0, 1745, 1524, 831, 1240, 959, 2596, 403, 1589, 1374, 357, 579],
                [713, 1745, 0, 355, 920, 803, 1737, 851, 1858, 262, 940, 1453, 1260],
                [1018, 1524, 355, 0, 700, 862, 1395, 1123, 1584, 466, 1056, 1280, 987],
                [1631, 831, 920, 700, 0, 663, 1021, 1769, 949, 796, 879, 586, 371],
                [1374, 1240, 803, 862, 663, 0, 1681, 1551, 1765, 547, 225, 887, 999],
                [2408, 959, 1737, 1395, 1021, 1681, 0, 2493, 678, 1724, 1891, 1114, 701],
                [213, 2596, 851, 1123, 1769, 1551, 2493, 0, 2699, 1038, 1605, 2300, 2099],
                [2571, 403, 1858, 1584, 949, 1765, 678, 2699, 0, 1744, 1645, 653, 600],
                [875, 1589, 262, 466, 796, 547, 1724, 1038, 1744, 0, 679, 1272, 1162],
                [1420, 1374, 940, 1056, 879, 225, 1891, 1605, 1645, 679, 0, 1017, 1200],
                [2145, 357, 1453, 1280, 586, 887, 1114, 2300, 653, 1272, 1017, 0, 504],
                [1972, 579, 1260, 987, 371, 999, 701, 2099, 600, 1162, 1200, 504, 0],
            ]
    else:
        mat = file_to_graph(sys.argv[1])

    model = ACO()
    print(model.solv(mat)[0])
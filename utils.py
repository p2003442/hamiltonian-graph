def cycle_cost(mat,cycle):
    return sum(mat[cycle[i-1]][cycle[i]] for i in range(len(cycle)))

def distance(x1,y1,x2,y2) -> float:
    return ((x1-x2)**2 + (y1-y2)**2)**0.5

def file_to_graph(file_path) -> list[list[float]]:
    
    with open(file_path) as file:
        content = file.read().splitlines()
        n = 0
        while content[n] !=  "NODE_COORD_SECTION":
            n+=1
        node_coordinates = []
        n+=1
        while content[n].strip() != 'EOF':
            line  = content[n].split()
            node_coordinates.append([float(line[1]),float(line[2])])
            n+=1
    mat = [[0 for _ in range(len(node_coordinates))] for _ in range(len(node_coordinates))]
    for i in range(len(node_coordinates)):
        for j in range(len(node_coordinates)):
            mat[i][j] = int(distance(node_coordinates[i][0],node_coordinates[i][1],node_coordinates[j][0],node_coordinates[j][1])*1000000)
    return mat
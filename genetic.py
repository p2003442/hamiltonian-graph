import random as r
from utils import cycle_cost


class Individu():
    def __init__(self,mat,perm):
        self.cycle = perm
        self.cost = cycle_cost(mat,self.cycle)
    def __gt__(self,other):
        return self.cost > other.cost
    def __lt__(self,other):
        return self.cost < other.cost
    def __str__(self):
        return f"cycle : {self.cycle} , coût : {self.cost}"
    def __eq__(self, other):
        return self.cycle == other.cycle


def first_generation(mat,nb_ind) -> list[Individu]:
    nb_node = len(mat)
    return [Individu(mat,r.sample(range(nb_node),k=nb_node)) for _ in range(nb_ind)]

#renvoie les proportion meilleur dans l'ordre croissant de coût
def selection(gen:list[Individu],proportion) -> list[Individu]:
    gen.sort()
    return gen[:int(len(gen)*proportion)]


def croisement(x:Individu,y:Individu,mat) -> Individu:
    new_cycle = x.cycle[0:len(x.cycle)//2].copy()
    new_cycle += [i for i in y.cycle if i not in new_cycle]
    return Individu(mat,new_cycle)

def mutation(x:Individu,mat) -> Individu:
    new_cycle = x.cycle.copy()
    i = r.randint(0,len(new_cycle)-1)
    j = r.randint(0,len(new_cycle)-1)
    a = min(i,j)
    b = max(i,j)
    new_cycle[a:b+1] = list(reversed(new_cycle[a:b+1]))
    return Individu(mat,new_cycle)

def gen_algo(mat,nb_ind,nb_generation,p_mutation) -> list[Individu]:
    gen = first_generation(mat,nb_ind)
    best_individu :Individu= gen[0]
    res = []
    for n in range(nb_generation):
        selec = selection(gen,len(gen)**0.5/len(gen))
        if selec[0] < best_individu:
            best_individu = selec[0]
        res.append(best_individu)
        print(f"meilleur coût {best_individu.cost}, génération {n}")
        croisements = selec.copy()
        for parent1 in selec:
            for parent2 in selec:
                croisements.append(croisement(parent1,parent2,mat))
        for i in range(len(croisements)):
            if r.random() < p_mutation:
                croisements[i] = mutation(croisements[i],mat)
        gen = croisements
    res.append(min(best_individu,min(gen)))
    return res

def alea_gen(mat,n):
    return min(first_generation(mat,n))

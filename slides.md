---
mdc: true
theme: bricks
---

# Introduction

<br/><br/>
Valentin Cuzin-Rambaud  
Jean-Luc Guillaume  
Abdurrahman Sen  

---

## Problème
voyageur de commerce (TSP)
![alt text](./image.png){width=85%}

---
layout: section
---

## Modélisation

---

### Modélisation en Vehicle Routing Problem (de OR Tools)

<br/><br/>

- Matrice de distances entre noeuds
  - avec une fonction de coût des arêtes
- Nombre de véhicules
- Nombre de dépôts

---

### Modélisation en CSP

<br/><br/>

- X = ensemble de Xi, un pour chaque noeud du graphe
  - Représente le noeud traversé à l'étape i du parcours
- D = ensemble d'entiers entre 0 et n-1, avec n le nombre de noeuds dans le graphe
- C = pour tout i, j (de 0 à n-1) différents, Xi =/= Xj
  - On ne passe pas deux fois par un même noeud

---

## Représentation

Matrice de distance

```py
[
  [0, 2451, 713, 1018, 1631, 1374, 2408, 213, 2571, 875, 1420, 2145, 1972],
  [2451, 0, 1745, 1524, 831, 1240, 959, 2596, 403, 1589, 1374, 357, 579],
  [713, 1745, 0, 355, 920, 803, 1737, 851, 1858, 262, 940, 1453, 1260],
  [1018, 1524, 355, 0, 700, 862, 1395, 1123, 1584, 466, 1056, 1280, 987],
  [1631, 831, 920, 700, 0, 663, 1021, 1769, 949, 796, 879, 586, 371],
  [1374, 1240, 803, 862, 663, 0, 1681, 1551, 1765, 547, 225, 887, 999],
  [2408, 959, 1737, 1395, 1021, 1681, 0, 2493, 678, 1724, 1891, 1114, 701],
  [213, 2596, 851, 1123, 1769, 1551, 2493, 0, 2699, 1038, 1605, 2300, 2099],
  [2571, 403, 1858, 1584, 949, 1765, 678, 2699, 0, 1744, 1645, 653, 600],
  [875, 1589, 262, 466, 796, 547, 1724, 1038, 1744, 0, 679, 1272, 1162],
  [1420, 1374, 940, 1056, 879, 225, 1891, 1605, 1645, 679, 0, 1017, 1200],
  [2145, 357, 1453, 1280, 586, 887, 1114, 2300, 653, 1272, 1017, 0, 504],
  [1972, 579, 1260, 987, 371, 999, 701, 2099, 600, 1162, 1200, 504, 0],
]
```

---
layout: center
zoom: 1.2
---

## Solveur

<br/>

## OR-tools

![OR-tools Logo](./images/orLogo.png)

---

## Benchmark

auteur du benchmark : Gerhard Reinelt à l'Universität Heidelberg

8 graphes complets
- matrix 0 avec 51 nodes
- matrix 1 avec 76 nodes
- matrix 2 avec 100 nodes
- matrix 3 avec 280 nodes
- matrix 4 avec 493 nodes
- matrix 5 avec 654 nodes
- matrix 6 avec 1291 nodes
- matrix 7 avec 13 nodes provenant de la doc d'OR-tools

---
layout: section
---

## Methodes Complètes

---

### OR-tools paramétrage par défaut

![Résultats par défaut](./images/results_ortools_default.png){width=60%}

---

### OR-tools paramétré

![Résultats paramétrés](./images/results_ortools_tuned.png){width=85%}

---

### OR-tools, TSP vu comme un CSP

<br/>

| Graphe  | Statut : 5s | Distance : 5s | Statut : 15s | Distance : 15s | Statut : 30s | Distance : 30s     | Statut : 60s | Distance : 60s     |
| ------- | ----------- | ------------- | ------------ | -------------- | ------------ | ------------------ | ------------ | ------------------ |
| goo13   | Opt         | 7293          | Opt          | 7293           | Opt          | 7293               | Opt          | 7293               |
| eil51   | Unk         | &infin;       | Unk          | &infin;        | Feas         | 1.3x10<sup>9</sup> | Feas         | 1.0x10<sup>9</sup> |
| eil76   | Unk         | &infin;       | Unk          | &infin;        | Unk          | &infin;            | Feas         | 1.9x10<sup>9</sup> |
| kroA100 | Unk         | &infin;       | Unk          | &infin;        | Unk          | &infin;            | Unk          | &infin;            |

---
layout: section
---

## Methodes Incompletes

---

### Algo glouton

<br/>

- Initialisation : noeuds parcourus = ø
- On prend un noeud de départ au hasard et on l'ajoute  au noeuds parcourus 
- On choisi l'arrête de coût minimal provenant de ce noeud (*)
- On prend comme nouveau l'autre noeud de l'arrête et on l'ajoute au noeuds parcourus
- On choisi l'arrête suivante de poids minimale provenant de ce noeud qui n'ont pas pour autre noeud un noeud déjà parcouru
- On repart sur (*) si on a encore des noeuds qui n'appartiennent pas au chemin

---
zoom: 0.75
---

### Résultat

| Taille graphe | Coût du cycle obtenu | Coût solution or-tools |
| ------------- | -------------------- | --------------------------- |
| 13            | 8114                 | 7293                       |
| 51            | 540 026 939          | 431 672 929                |
| 76            | 626 001 542          | 561 769 271                |
| 100           | 25 410 855 672       | 21 962 762 959             |
| 280           | 3 302 577 939        | 2 740 436 892              |
| 493           | 42 210 918 172       | 36 565 197 577             |
| 654           | 41 225 285 090       | 34 998 948 205             |
| 1291          | 61 539 369 878       | 53 858 964 972             |

---

### Optimisation 2-opt

![exemple2opt](./images/example2opt.png)

---
zoom: 0.8
---

### Résultat

| Taille graphe | Coût du cycle obtenu  greedy | Coût du cycle obtenu  greedy + 2opt | Coût solution or-tools |
| ------------- | ---------------------------- | ----------------------------------- | --------------------------- |
| 13            | 8114                         | 7462                                | 7293                        |
| 51            | 540 026 939                  | 441 923 711                         | 431 672 929                 |
| 76            | 626 001 542                  | 601 318 063                         | 561 769 271                 |
| 100           | 25 410 855 672               | 21 923 431 837                      | 21 962 762 959              |
| 280           | 3 302 577 939                | 2 939 573 853                       | 2 740 436 892               |
| 493           | 42 210 918 172               | 38 516 958 220                      | 36 565 197 577              |
| 654           | 41 225 285 090               | 35 692 147 300                      | 34 998 948 205              |
| 1291          | 61 539 369 878               | 58 535 091 675                      | 53 858 964 972              |

---

### Ant Colonisation Optimisation

<br/>

#### Paramétrage

- v_init : noeud de départ de la fourmis par défaut à None
- it : nombre d'itération par défaut à 2000
- time : temps borné par défaut à None
- nb_ant : nombre de fourmis par défaut à 100
- fact_pherom : facteur phéromonale par défaut à 10
- fact_heurist : facteur heuristique par défaut à 20
- tau_evap : taux d'évaporation par défaut à 0.01
- min_pherom : minimum de pheromones par défaut à 0.01
- max_pherom : maximum fr pheromones par défaut à 0.9

---
zoom: 0.9
---

**paramètres** : v_init aléatoire, 2000 it, 100 nb_ant, 2 fact_heurist, 0.01 min_pherom, 0.9 max_pherom, 0.01 tau_evap
![alt text](./images/image-6.png)


---
zoom: 0.9
---

**paramètres** : v_init aléatoire, 1000 it, 100 nb_ant, 2 fact_heurist, 0.01 min_pherom, 0.9 max_pherom, 0.01 tau_evap
![alt text](./images/image-2.png)

---
zoom: 0.9
---

**paramètres** : v_init aléatoire, 2000 it, 100 nb_ant, 1 fact_pherom, 2 fact_heurist, 0.01 min_pherom, 0.9 max_pherom,
![alt text](./images/image-4.png)

---
zoom: 0.9
---

**paramètres** : v_init aléatoire, 2000 it, 100 nb_ant, 1 fact_pherom, 2 fact_heurist, 0.9 max_pherom, 0.01 tau_evap
![alt text](./images/image-5.png)

---

#### Resultat
pour une minute de timeout:

- matrix 0, chemin trouvé de longueur de 458 769 821
- matrix 1, chemin trouvé de longueur de 588 792 902
- matrix 2, chemin trouvé de longueur de 24 469 465 946
- matrix 3, chemin trouvé de longueur de 3 266 266 299
- matrix 4, chemin trouvé de longueur de 47 468 520 926 (unique solution)
- matrix 5, pas de solution trouvé  
- matrix 6, pas une solution trouvé
- matrix 7, chemin trouvé de longueur de 7293

---

![alt text](./images/image-7.png)

---

![alt text](./images/image-3.png)

---

![alt text](./images/image-8.png)

---
layout: image-right

source: ./images/croisement.png
---

### Algo Génétique

On génère N chemin aux hasard
3 étapes qu'on répète : 
- sélection : on choisi une partie des meilleurs cycles
- on les croise pour obtenir les "enfants" des cycles
- la nouvelle génération consiste des parents + enfants qui subiront une mutation avec une proba donnée

---
zoom: 0.8
---

### Résultats
En faisant 100 générations avec 1000 individus par génération
| Taille graphe | Coût du cycle obtenu | Coût du cycle obtenu  greedy + 2opt | Coût solution or-tools |
| ------------- | -------------------- | ----------------------------------- | --------------------------- |
| 13            | 7569                 | 7462                                | 7293                        |
| 51            | 477 949 750          | 441 923 711                         | 431 672 929                 |
| 76            | 775 558 430          | 601 318 063                         | 561 769 271                 |
| 100           | 44 317 634 045       | 21 923 431 837                      | 21 962 762 959              |
| 280           | 15 043 361 412       | 2 939 573 853                       | 2 740 436 892               |
| 493           | 268 823 544 657      | 38 516 958 220                      | 36 565 197 577              |
| 654           | 1 141 594 552 251    | 35 692 147 300                      | 34 998 948 205              |
| 1291          | 1 366 593 636 685    | 58 535 091 675                      | 53 858 964 972              |

---

![alt text](./images/51algogen.png)

---

![alt text](./images/76algogen.png)

---

#### Temps d'éxécution (en secondes) en fonction de la taille du graphe
![alt text](./images/timeexec.png)

---
layout: cover
---

# Conclusion


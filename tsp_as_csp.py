from ortools.sat.python import cp_model
from utils import file_to_graph
import os
import csv

def tsp_csp(distance_matrix):
    num_cities = len(distance_matrix)
    model = cp_model.CpModel()

    # Decision variables: path[i] is the city visited at position i in the path
    path = [model.NewIntVar(0, num_cities - 1, f'path_{i}') for i in range(num_cities)]

    # Ensure all cities are visited exactly once
    model.AddAllDifferent(path)

    # Flatten the distance matrix for use with AddElement
    flattened_distances = [
        distance_matrix[row][col]
        for row in range(num_cities)
        for col in range(num_cities)
    ]

    # Segment distances
    segment_distances = []
    max_dist = max(max(row) for row in distance_matrix)
    for i in range(num_cities):
        segment_distance = model.NewIntVar(0, max_dist, f'segment_distance_{i}')
        segment_distances.append(segment_distance)

        # Map path[i] and path[i+1] into a flattened index
        from_city = path[i]
        to_city = path[(i + 1) % num_cities]  # Ensure cyclic path
        index = model.NewIntVar(0, num_cities * num_cities - 1, f'index_{i}')
        model.Add(index == from_city * num_cities + to_city)

        # Use AddElement to set the segment distance
        model.AddElement(index, flattened_distances, segment_distance)

    # Total distance
    total_distance = model.NewIntVar(0, sum(max(row) for row in distance_matrix), 'total_distance')
    model.Add(total_distance == sum(segment_distances))

    # Objective: Minimize the total distance
    model.Minimize(total_distance)

    # Solve the model
    solver = cp_model.CpSolver()
    solver.parameters.max_time_in_seconds = 5.0
    solver.parameters.num_search_workers = 8
    status = solver.Solve(model)

    # Output the solution
    if status == cp_model.OPTIMAL or status == cp_model.FEASIBLE:
        print('Solution status:', solver.StatusName(status))
        print('Total distance =', solver.Value(total_distance))
        solution = [solver.Value(path[i]) for i in range(num_cities)]
        print('Path:', solution)
        return (solver.StatusName(status), solver.Value(total_distance), solution)
    else:
        print('No solution found.')
        return (solver.StatusName(status), None, None)

graphs_folder = "graphs/smallerBenchmarks"
results = []

for filename in os.listdir(graphs_folder):
    if filename.endswith(".tsp"):
        filepath = os.path.join(graphs_folder, filename)
        distance_matrix = file_to_graph(filepath)
        print(f"Solving TSP for {filename}")
        status, distance, path = tsp_csp(distance_matrix)
        results.append({
                "filename": filename,
                "status": status,
                "total_distance": distance,
                "path": path
            })

# Example distance matrix
distance_matrix = [
    [0, 2451, 713, 1018, 1631, 1374, 2408, 213, 2571, 875, 1420, 2145, 1972],
    [2451, 0, 1745, 1524, 831, 1240, 959, 2596, 403, 1589, 1374, 357, 579],
    [713, 1745, 0, 355, 920, 803, 1737, 851, 1858, 262, 940, 1453, 1260],
    [1018, 1524, 355, 0, 700, 862, 1395, 1123, 1584, 466, 1056, 1280, 987],
    [1631, 831, 920, 700, 0, 663, 1021, 1769, 949, 796, 879, 586, 371],
    [1374, 1240, 803, 862, 663, 0, 1681, 1551, 1765, 547, 225, 887, 999],
    [2408, 959, 1737, 1395, 1021, 1681, 0, 2493, 678, 1724, 1891, 1114, 701],
    [213, 2596, 851, 1123, 1769, 1551, 2493, 0, 2699, 1038, 1605, 2300, 2099],
    [2571, 403, 1858, 1584, 949, 1765, 678, 2699, 0, 1744, 1645, 653, 600],
    [875, 1589, 262, 466, 796, 547, 1724, 1038, 1744, 0, 679, 1272, 1162],
    [1420, 1374, 940, 1056, 879, 225, 1891, 1605, 1645, 679, 0, 1017, 1200],
    [2145, 357, 1453, 1280, 586, 887, 1114, 2300, 653, 1272, 1017, 0, 504],
    [1972, 579, 1260, 987, 371, 999, 701, 2099, 600, 1162, 1200, 504, 0],
]
print(f"Solving TSP for Google Example")
tsp_csp(distance_matrix)

# Save results to CSV
with open('tsp_as_csp_results.csv', mode='w', newline='') as file:
    writer = csv.DictWriter(file, fieldnames=["filename", "status", "total_distance", "path"])
    writer.writeheader()
    for result in results:
        writer.writerow(result)


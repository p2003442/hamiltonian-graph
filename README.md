# Hamiltonian graph

## Groupe

Valentin Cuzin-Rambaud  
Jean-Luc Guillaume  
Abdurrahman Sen  

## Contenu

Ce répertoire contient tous les fichiers qui nous ont servi pour le projet.  

Vous trouverez les __diapositives__ au format pdf dans le fichier [slides.pdf](./slides.pdf).  
Un __Notebook Python__ regroupe l'ensemble de nos travaux et permet de lancer les différentes configurations et algorithmes testés. c'est le fichier [benchmark.ipynb](./benchmark.ipynb).  

Vous trouverez certains résultats directement dans le Notebook, d'autres au format CSV dans le répertoire [results/](./results/)

## Rappels sujet

- Présentation 1 : 15mn (+5mn questions) –> 01/10
- Présentation 2 : 20mn (+10mn questions) –> à partir du 26/11
- 40%

- [OR-tools doc](https://developers.google.com/optimization/routing/tsp?hl=fr)

- [page wiki du problème](https://en.wikipedia.org/wiki/Travelling_salesman_problem)

- [la data pour le benchmark](http://comopt.ifi.uni-heidelberg.de/software/TSPLIB95/)

![alt text](images/image.png)

## Présentation 1

### Problème choisi

Nous avons sélectionné le problème du Voyageur de Commerce (_Traveling Salesman Problem_ en anglais), soit la recherche du plus court cycle hamiltonien dans un graphe.  

### Solveur choisi

Nous avons décidé de partir sur le solveur OR-tools de Google, simplement car il est utilisable en python.

### Modélisation des instances

L'exemple montré dans OR-tools se base sur une représentation du graphe par un matrice de distance entre chaque noeud.  
Par exemple :

![Exemple de modélisation de graphe](images/matrice_distance_ortools.png)

OR-tools peut résoudre un problème de routage de plusieurs véhicules dans un graphe, mais cela ne nous intéresse pas, puisque 1 voiture suffit pour trouver le plus court chemin dans un graphe.

### Benchmark

Nous avons trouvé plusieurs jeux de test, mais la page _Discrete and Combinatorial Optimization_ de Gerhard Reinelt à l'Universität Heidelberg contient beaucoup d'exemples de graphes, allant de 16 à 85 900 noeuds.  

Cependant les graphes sont représentés par l'ensemble des coordonnées des noeuds. Il nous incombe donc de traduire ces instances en calculant les distances euclidiennes entre chaque noeud.

![Exemple d'instance de graphe](images/exemple_graphe.png)

Nous avons également la solution optimale de chacune de instances du problème [disponible ici](http://comopt.ifi.uni-heidelberg.de/software/TSPLIB95/STSP.html)

### Retours de présentation 1

Pistes à approfondir :

- Comment est-ce que OR-tools choisit un point de départ (noeud) différent ?
- Où sont les variables ?
  - Créer une modélisation type CSP et la donner à OR-tools (pas en tant que TSP)
    - Comparer les résultats entre la version CSP et matrice de distance
- Regarder les autres méthodes de recherches (approches incomplètes, en changeant la stratégie de recherche par exemple) et comparer avec les méthodes complètes
- Configurer OR-tools pour prouver l'optimalité de la solution trouvée.

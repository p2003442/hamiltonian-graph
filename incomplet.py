from math import inf
import random as r

def greedy(mat):
    node_to_visit = set(range(len(mat)))
    initial_node = r.randint(0,len(mat)-1)
    res = []
    node_to_visit.remove(initial_node)
    last_node_visited = initial_node

    for _ in range(len(mat)-1):
        minimum = inf
        min_node = None
        for n in node_to_visit:
            if mat[last_node_visited][n] <= minimum:
                minimum = mat[last_node_visited][n]
                min_node = n
        node_to_visit.remove(min_node)
        res.append(min_node)
        last_node_visited = min_node
    return res

# attention 2-opt ainsi implémenté ne marche que si le graphe n'est pas dirigé
def opt_2(mat,cycle):
    improve = True
    while improve:
        improve = False
        for i in range(len(cycle)-1):
            for j in range(i+2,len(cycle)-1):
                if mat[cycle[i]][cycle[i+1]] + mat[cycle[j]][cycle[j+1]] > mat[cycle[i]][cycle[j]] + mat[cycle[i+1]][cycle[j+1]]:
                    cycle = cycle[:i+1] + list(reversed(cycle[i+1:j+1])) +cycle[j+1:]
                    improve = True
    return cycle